#include "utils.h"

std::string bigUnsignedToString( const BigUnsigned &x )
{
	std::ostringstream ret;
	ret << x;
	return ret.str();
}

std::string bigIntegerToString( const BigInteger &x )
{
	std::ostringstream ret;
	ret << x;
	return ret.str();
}

BigUnsigned stringToBigUnsigned( const std::string &s )
{
	return BigUnsigned( s );
}

BigInteger stringToBigInteger( const std::string &s )
{
	return BigInteger( s );
}