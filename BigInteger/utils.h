#pragma once
#include "BigInteger.h"
#include <string>
#include <sstream>

std::string bigUnsignedToString( const BigUnsigned &x );
std::string bigIntegerToString( const BigInteger &x );
BigUnsigned stringToBigUnsigned( const std::string &s );
BigInteger stringToBigInteger( const std::string &s );
