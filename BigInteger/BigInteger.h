#pragma once
#include "BigUnsigned.h"

class BigInteger
{
public:
	BigInteger();
	BigInteger( BigUnsigned );
	BigInteger( const std::string& );
	BigInteger( const char* );
	template <typename T>
	BigInteger( T x, typename 
		std::enable_if<std::is_integral<T>::value>::type* = nullptr );
	~BigInteger();

	BigInteger& operator+= ( const BigInteger& );
	BigInteger& operator-= ( const BigInteger& );
	BigInteger& operator*= ( const BigInteger& );
	BigInteger& operator/= ( const BigInteger& );
	BigInteger& operator%= ( const BigInteger& );
	BigInteger& operator<<= ( const BigInteger& );
	BigInteger& operator>>= ( const BigInteger& );

	friend BigInteger operator+ ( const BigInteger&, const BigInteger& );
	friend BigInteger operator- ( const BigInteger&, const BigInteger& );
	friend BigInteger operator* ( const BigInteger&, const BigInteger& );
	friend BigInteger operator/ ( const BigInteger&, const BigInteger& );
	friend BigInteger operator% ( const BigInteger&, const BigInteger& );
	friend BigInteger operator<< ( const BigInteger&, const BigInteger& );
	friend BigInteger operator>> ( const BigInteger&, const BigInteger& );

	BigInteger operator+ ();
	BigInteger operator- ();
	BigInteger& operator++ ();
	BigInteger operator++ ( int );
	BigInteger& operator-- ();
	BigInteger operator-- ( int );

	friend bool operator== ( const BigInteger&, const BigInteger& );
	friend bool operator!=( const BigInteger&, const BigInteger& );
	friend bool operator< ( const BigInteger&, const BigInteger& );
	friend bool operator> ( const BigInteger&, const BigInteger& );
	friend bool operator<= ( const BigInteger&, const BigInteger& ); 
	friend bool operator>= ( const BigInteger&, const BigInteger& ); 

	friend std::istream& operator>> ( std::istream&, BigInteger& );
	friend std::ostream& operator<< ( std::ostream&, const BigInteger& );

	friend BigInteger pow( const BigInteger&, const BigInteger& );
	friend BigInteger sqrt( const BigInteger& );	
	friend BigInteger abs( const BigInteger& );

	bool is_zero() const;
#ifdef __GNUC__ 
	explicit operator bool() const;	
#endif	
	operator BigUnsigned() const;		
private:
	bool sign;
	BigUnsigned mag;
};