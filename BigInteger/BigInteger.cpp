#include "BigInteger.h"

BigInteger::BigInteger(): 
	sign( false ), mag()
{}

BigInteger::BigInteger( BigUnsigned u ): 
	sign( false ), mag( u )
{}

BigInteger::BigInteger( const std::string& str ):
	sign( false )
{
	if (str.front() == '-')
	{
		sign = true;
		mag = str.c_str() + 1;
	}
	else
		mag = str;
}

BigInteger::BigInteger( const char* str ):
	sign( false )
{
	if (*str == '-')
	{
		sign = true;
		mag = str + 1;
	}
	else
		mag = str;
}

template <typename T>
BigInteger::BigInteger( T x, typename
					   std::enable_if<std::is_integral<T>::value>::type* ):
sign( x < 0 ), mag( abs( x ) )
{}

BigInteger::~BigInteger()
{}

BigInteger& BigInteger::operator+= ( const BigInteger& rhs )
{
	if ( sign == rhs.sign )
	{
		mag = mag + rhs.mag;
	}
	else if ( mag >= rhs.mag )
	{
		sign = (mag == rhs.mag ? false : sign);
		mag = mag - rhs.mag;		
	}
	else
	{
		mag = rhs.mag - mag;
		sign = rhs.sign;
	}
	return *this;	
}

BigInteger& BigInteger::operator-= ( const BigInteger& rhs )
{
	bool f = sign && rhs.sign;
	if ( sign != rhs.sign )
	{
		mag = mag + rhs.mag;
	}
	else if ( mag < rhs.mag )
	{
		mag = rhs.mag - mag;
		sign = true ^ f;
	}
	else
	{
		mag = mag - rhs.mag;
		sign = false ^ f;
	}
	return *this;	
}

BigInteger& BigInteger::operator*= ( const BigInteger& rhs )
{
	mag = mag * rhs.mag;
	sign ^= rhs.sign;
	return *this;
}

BigInteger& BigInteger::operator/= ( const BigInteger& rhs )
{
	mag = mag / rhs.mag;
	sign ^= rhs.sign;
	return *this;
}

BigInteger& BigInteger::operator%= ( const BigInteger& rhs )
{
	mag = mag % rhs.mag;
	sign ^= rhs.sign;
	return *this;
}

BigInteger& BigInteger::operator<<= ( const BigInteger& rhs )
{
	if ( rhs > 0 )
		mag <<= rhs.mag;
	return *this;
}

BigInteger& BigInteger::operator>>= ( const BigInteger& rhs )
{
	if ( rhs > 0 )
		mag >>= rhs.mag;
	return *this;
}

BigInteger operator+ ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret += rhs;
}

BigInteger operator- ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret -= rhs;
}

BigInteger operator* ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret *= rhs;
}

BigInteger operator/ ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret /= rhs;
}

BigInteger operator% ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret %= rhs;
}

BigInteger operator<< ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret <<= rhs;
}

BigInteger operator>> ( const BigInteger& lhs, const BigInteger& rhs )
{
	BigInteger ret( lhs );
	return ret >>= rhs;
}

BigInteger BigInteger::operator+ ()
{
	return *this;
}

BigInteger BigInteger::operator- ()
{
	BigInteger ret(*this);
	if (!ret.is_zero())
		ret.sign = !ret.sign;
	return ret;
}

BigInteger& BigInteger::operator++ ()
{
	if ( !sign )
		++mag;
	else 
	{
		--mag;
		if ( is_zero() )
			sign = false;
	}
	return *this;
}

BigInteger BigInteger::operator++ ( int )
{
	BigInteger ret( *this );
	++( *this );
	return ret;
}

BigInteger& BigInteger::operator-- ()
{
	if ( sign )
		++mag;
	else if ( is_zero() )
	{
		++mag;
		sign = true;
	}
	else	
		--mag;		
	return *this;
}

BigInteger BigInteger::operator-- ( int )
{
	BigInteger ret( *this );
	--( *this );
	return ret;
}

bool operator== ( const BigInteger& lhs, const BigInteger& rhs ) 
{
	return lhs.mag == rhs.mag && lhs.sign == rhs.sign;
}

bool operator!= ( const BigInteger& lhs, const BigInteger& rhs ) 
{
	return !( lhs == rhs );
}

bool operator< ( const BigInteger& lhs, const BigInteger& rhs ) 
{
	if ( lhs.sign != rhs.sign )
		return lhs.sign;
	else if ( !lhs.sign )
		return lhs.mag < rhs.mag;
	else 
		return rhs.mag < lhs.mag;		
}

bool operator> ( const BigInteger& lhs, const BigInteger& rhs )  
{
	return rhs < lhs;
}

bool operator<= ( const BigInteger& lhs, const BigInteger& rhs ) 
{
	return !( rhs < lhs );
}

bool operator>= ( const BigInteger& lhs, const BigInteger& rhs ) 
{
	return !( lhs < rhs );
}

std::istream& operator>> ( std::istream& stream, BigInteger& rhs )
{
	std::string str;
	stream >> str;
	rhs = BigInteger( str );
	return stream;	
}

std::ostream& operator<< ( std::ostream& stream, const BigInteger& rhs )
{
	if ( rhs.sign )
		stream << '-';
	stream << rhs.mag;
	return stream;
}


#ifdef __GNUC__ 	
BigInteger::operator bool() const
{
	return is_zero();
}
#endif

BigInteger pow( const BigInteger& lhsn, const BigInteger& rhsn ) 
{
	BigInteger lhs( lhsn ), rhs( rhsn );
	if ( rhs.sign )
		throw "BigInteger::pow: negative power"; 
	BigInteger ret;
	ret.mag = pow( lhs.mag, rhs.mag );
	if ( lhs.sign )
		ret.sign = rhs % 2 != 0;
	return ret;
}

BigInteger sqrt( const BigInteger& x )
{
	BigInteger ret;
	if ( !x.sign )
		ret.mag = sqrt( x.mag );
	else
		throw "BigInteger::sqrt: root of a negative"; 
	return ret;
}

BigInteger abs( const BigInteger& rhs )
{
	BigInteger ret( rhs );
	if ( ret.sign )
		ret.sign = !ret.sign;
	return ret;
}

bool BigInteger::is_zero() const
{
	return mag.is_zero();
}

BigInteger::operator BigUnsigned() const
{
	return mag;
}