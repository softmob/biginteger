#pragma once
#include <iostream>
#include <algorithm>
#include <utility>
#include <iterator>
#include <memory>
#include <type_traits>
#include <stdexcept>
#include <cstddef>

#ifdef __GNUC__
#include <initializer_list>
#endif

using std::rel_ops::operator!=;
using std::rel_ops::operator>;
using std::rel_ops::operator<=;
using std::rel_ops::operator>=;

template <class T, class Allocator = std::allocator<T>> 
class mvector
{
public:
	typedef T value_type;
	typedef Allocator allocator_type;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T* iterator;
	typedef const T* const_iterator; 
	typedef std::reverse_iterator<iterator> reverse_iterator; 
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator; 

	explicit mvector( const Allocator& alloc = Allocator() ):
	m_size( 0 ), m_capacity( m_size ), 
		m_alloc( alloc ), m_arr( m_alloc.allocate( m_capacity ) )
	{} 

	mvector( size_type count, 
		const T& value,
		const Allocator& alloc = Allocator() ):
	m_size( count ), m_capacity( m_size ), 
		m_alloc( alloc ), m_arr( m_alloc.allocate( m_capacity ) )
	{
		std::uninitialized_fill_n( begin(), m_size, value );
	}

	explicit mvector( size_type count ):
	m_size( count ), m_capacity( m_size ), 
		m_alloc(), m_arr( m_alloc.allocate( m_capacity ) )
	{
		std::uninitialized_fill_n( begin(), m_size, T() );
	}

	template <class InputIterator>
	mvector( InputIterator first, InputIterator last, 
		const Allocator& alloc = Allocator(),
		typename std::enable_if<!std::is_integral
		<InputIterator>::value>::type* = nullptr ):
	m_size( std::distance( first, last ) ), m_capacity( m_size ),
		m_alloc( alloc ), m_arr( m_alloc.allocate( m_capacity ) )
	{ 
		if ( begin() != nullptr )
			std::uninitialized_copy( first, last, begin() ); 
	}

	mvector( const mvector& other ):
		m_size( other.m_size ), m_capacity( m_size ), 
		m_alloc( other.m_alloc ), m_arr( m_alloc.allocate( m_capacity ) )
	{
		if ( begin() != nullptr )
			std::uninitialized_copy( other.begin(), other.end(), begin() ); 
	}

	mvector( const mvector& other, const Allocator& alloc ):
		m_size( other.m_size ), m_capacity( m_size ), 
		m_alloc( alloc ), m_arr( m_alloc.allocate( m_capacity ) )
	{
		if ( begin() != nullptr )
			std::uninitialized_copy( other.begin(), other.end(), begin() ); 
	}

	mvector( mvector&& other ):
		m_size( std::move(other.m_size) ), m_capacity( std::move(other.m_capacity) ), 
		m_alloc( std::move(other.m_alloc) ), m_arr( std::move(other.m_arr) )
	{
		other.m_size = 0;   
		other.m_capacity = 0;
		other.m_arr = nullptr;
	}

	mvector( mvector&& other, const Allocator& alloc ):
		m_size( std::move(other.m_size) ), m_capacity( std::move(other.m_capacity) ), 
		m_alloc( alloc ), m_arr( std::move(other.m_arr) )
	{
		other.m_size = 0;   
		other.m_capacity = 0;
		other.m_arr = nullptr;
	}

	~mvector()
	{
		clear();
		if ( m_arr != nullptr && m_capacity )
			m_alloc.deallocate( m_arr, m_capacity );		
		m_capacity = 0; 
		m_arr = nullptr; 
	}

	mvector& operator= ( const mvector& other )
	{
		if ( this != &other )
			mvector( other ).swap( *this ); 
		return *this;
	}

	mvector& operator= ( mvector&& other )
	{
		if ( this != &other )
		{
			m_size = std::move( other.m_size );
			m_capacity = std::move( other.m_capacity );
			m_alloc = std::move( other.m_alloc );
			m_arr = std::move( other.m_arr );
			other.m_size = 0;   
			other.m_capacity = 0;
			other.m_arr = nullptr;
		}
		return *this;
	}

	void assign( size_type count, const T& value )
	{
		mvector( count, value ).swap( *this ); 
	}

	template <class InputIterator>
	void assign( InputIterator first, InputIterator last )
	{
		mvector( first, last ).swap( *this ); 
	}

	allocator_type get_allocator() const
	{
		return m_alloc;
	}

	reference at( size_type pos )
	{
		if ( pos >= m_size )
			throw std::out_of_range( "vector subscript out of range." );
		return m_arr[pos];
	}

	const_reference at( size_type pos ) const
	{
		if ( pos >= m_size )
			throw std::out_of_range( "vector subscript out of range." );
		return m_arr[pos];
	}

	reference operator[] ( size_type pos )
	{ 
		return m_arr[pos];
	}

	const_reference operator[] ( size_type pos ) const
	{
		return m_arr[pos];
	}

	reference front()
	{
		return *begin();
	}

	const_reference front() const
	{
		return *begin();
	}

	reference back()
	{
		return *( end() - 1 );
	}

	const_reference back() const
	{
		return *( end() - 1 );
	}

	pointer data()
	{
		return m_arr;
	}

	const_pointer data() const
	{
		return m_arr;
	}

	iterator begin()
	{
		return m_arr; 
	}

	const_iterator begin() const
	{
		return m_arr; 
	}

	const_iterator cbegin() const
	{
		return m_arr; 
	}

	iterator end()
	{
		return m_arr + m_size;
	}

	const_iterator end() const
	{ 
		return m_arr + m_size; 
	}

	const_iterator cend() const
	{ 
		return m_arr + m_size; 
	}

	reverse_iterator rbegin()
	{
		return reverse_iterator( end() );
	}

	const_reverse_iterator rbegin() const
	{
		return const_reverse_iterator( end() );
	}

	const_reverse_iterator crbegin() const
	{
		return const_reverse_iterator( end() );
	}

	reverse_iterator rend()
	{
		return reverse_iterator( begin() );
	}

	const_reverse_iterator rend() const
	{
		return const_reverse_iterator( begin() );
	}

	const_reverse_iterator crend() const
	{
		return const_reverse_iterator( begin() );
	}

	bool empty() const
	{
		return m_size == 0;
	}

	size_type size() const
	{
		return m_size;
	}

	size_type max_size() const
	{
		return size_type(-1) / sizeof(T);
	}

	void reserve( size_type size )
	{
		if ( capacity() < size ) 
		{
			size_type n = m_size;
			pointer newelements = m_alloc.allocate( size );
			std::uninitialized_copy( begin(), end(), newelements );
			clear();
			if ( m_arr && m_capacity )
				m_alloc.deallocate( m_arr, m_capacity );
			m_arr = newelements;
			m_size = n;
			m_capacity = size;
		}
	}

	size_type capacity() const
	{
		return m_capacity;
	}

	void shrink_to_fit()
	{
		if ( m_arr && size() != capacity() )
		{
			m_alloc.deallocate( end(), capacity() - size() );
			m_capacity = m_size;
		}
	}

	void clear() 
	{
		for ( size_t i = 0; i < m_size; ++i )
			m_alloc.destroy( m_arr + i );	
		m_size = 0;	 
	}

	iterator insert( const_iterator pos, const T& value )
	{
		return insert( pos, 1, value );
	}

	iterator insert( const_iterator pos, T&& value )
	{
		size_type n = std::distance( cbegin(), pos );       
		if ( size() + 1 > capacity() ) 
			reserve( 2 * ( size() + 1 ) ); 
		std::copy_backward( begin() + n, end(), end() + 1 ); 
		++m_size;
		back() = std::move( value );        
		return begin() + n;
	}

	iterator insert( const_iterator pos, size_type count, const T& value )
	{ 
		size_type n = std::distance( cbegin(), pos );
		if ( size() + count > capacity() ) 
			reserve( 2 * ( size() + count ) ); 
		std::copy_backward( begin() + n, end(), end() + count ); 
		std::fill( begin() + n, begin() + n + count, value );
		m_size += count;
		return begin() + n;
	}

	template <class InputIterator>
	typename std::enable_if<!std::is_integral<InputIterator>::value, iterator>::
		type insert( const_iterator pos, InputIterator first, InputIterator last)
	{ 
		size_type n = std::distance( cbegin(), pos );
		size_type count = std::distance ( first, last );
		if ( size() + count > capacity() )
			reserve( 2 * ( size() + count ) ); 
		std::copy_backward( begin() + n, end(), end() + count ); 
		std::copy( first, last, begin() + n ); 
		m_size += count;
		return begin() + n;
	}

	iterator erase( const_iterator pos )
	{
		size_type beg = std::distance( cbegin(), pos );
		if ( pos + 1 != cend() )
			std::copy( begin() + beg + 1, end(), begin() + beg );
		--m_size;
		destroy( end() );
		return begin() + beg;
	}

	iterator erase( const_iterator first, const_iterator last )
	{
		size_type size = std::distance( first, last );
		size_type beg = std::distance( cbegin(), first );
		iterator loc = std::copy( begin() + beg + size, end(), begin() + beg );
		for ( iterator i = end(); i != loc; )
			m_alloc.destroy(--i);
		return begin() + beg;
	}

	void push_back( const T& value )
	{
		insert( end(), value );
	}

	void push_back( T&& value )
	{
		insert( end(), std::move( value ) );
	}

	void pop_back()
	{
		if ( !empty() )
		{
			--m_size;
			m_alloc.destroy( end() );
		}
	}

	void resize( size_type count ) 
	{
		resize( count, T() );
	}

	void resize( size_type count, const value_type& value)
	{ 
		if ( count < size() ) 
			erase( begin() + count, end() );
		else if ( count > size() )
			insert( end(), count - size(), value );
	} 

	void swap( mvector& other )
	{ 
		if ( this != &other )
		{
			std::swap( m_size, other.m_size );
			std::swap( m_capacity, other.m_capacity );
			std::swap( m_alloc, other.m_alloc );
			std::swap( m_arr, other.m_arr );
		}
	}
#ifdef __GNUC__ 
public:
	mvector( std::initializer_list<T> init, 
		const Allocator& alloc = Allocator() ): 
	mvector( init.begin(), init.end(), alloc )
	{}

	iterator insert( const_iterator pos, std::initializer_list<T> ilist )
	{
		return m_range_insert( pos, ilist.begin(), ilist.end() );
	}

	iterator emplace( const_iterator pos )
	{
		return pos;
	}

	template< class... Args > 
	iterator emplace( const_iterator pos, T&& value, Args&&... args )
	{
		return emplace( insert( pos, std::move(value ) ), args... );
	}

	void emplace_back()
	{} 

	template< class... Args >
	void emplace_back( T&& value, Args&&... args )
	{
		push_back( std::move( value ) );
		emplace_back( args... );
	}
#endif
private:
	size_type m_size; 
	size_type m_capacity; 
	allocator_type m_alloc;
	pointer m_arr; 
};

template<class T, class Alloc>
bool operator== ( const mvector<T, Alloc>& lhs, 
				 const mvector<T, Alloc>& rhs )
{ 
	return (
		lhs.size() == rhs.size() 
		&& 
		std::equal( lhs.begin(), lhs.end(), rhs.begin() )
		);
}

template <class T, class Alloc>
bool operator< ( const mvector<T, Alloc>& lhs,
	const mvector<T, Alloc>& rhs )
{
	return std::lexicographical_compare( lhs.begin(), lhs.end(), 
		rhs.begin(), rhs.end() );
}

template <class T, class Alloc>
void swap( mvector<T, Alloc> &lhs, 
		  mvector<T, Alloc> &rhs )
{
	lhs.swap(rhs); 
}