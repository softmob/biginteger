#include <iostream>
#include <string>
#include "bigint.h"

int main(void)
{
	BigInteger a; 
	int b = 535;
	a = b;

	BigInteger c(a); 
	std::cout << c << std::endl;

	BigInteger d(-314159265);
	std::cout << d << std::endl;

	std::string s("3141592653589793238462643383279");
	BigInteger f = stringToBigInteger(s);
	std::string s2 = bigIntegerToString(f); 
	std::cout << f << std::endl;

	BigInteger g(314159), h(265);
	std::cout << (g + h) << '\n'
		<< (g - h) << '\n'
		<< (g * h) << '\n'
		<< (g / h) << '\n'
		<< (g % h) << std::endl;

	int maxPower = 10;
	BigUnsigned x(1), big314(314);
	for (int power = 0; power <= maxPower; power++) 
	{
		std::cout << "314^" << power << " = " << x << std::endl;
		x *= big314; 
	}
	return 0;
}