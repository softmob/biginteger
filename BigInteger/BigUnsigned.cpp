#include "BigUnsigned.h"

BigUnsigned::BigUnsigned()
{}

BigUnsigned::BigUnsigned( const std::string& str )
{
	for ( size_t i = 0; i < str.size(); ++i )
		if ( !isdigit( str[i] ) )
			return ;
	int digits = static_cast<int>( log10( base ) );
	for ( int i = str.size(); i > 0; i -= digits )
		if ( i < digits )
			v.push_back( atoi( str.substr( 0, i ).c_str() ) );
		else
			v.push_back( atoi( str.substr( i - digits, digits ).c_str() ) );
}

BigUnsigned::BigUnsigned( const char* str )
{	
	*this = BigUnsigned( std::string( str ) );
}

template <typename T>
BigUnsigned::BigUnsigned( T val , typename std::enable_if
						 <std::is_integral<T>::value>::type* )
{
	while ( val > 0 )
	{
		v.push_back( val % base );
		val /= base;
	}
}

BigUnsigned::~BigUnsigned()
{}

BigUnsigned& BigUnsigned::operator+= ( const BigUnsigned& rhs )
{
	value_type carry = 0;
	for ( size_t i = 0; i < std::max( v.size(), rhs.v.size() ) || carry; ++i )
	{
		if ( i == v.size() )
			v.push_back( 0 );
		v[i] += carry + ( i < rhs.v.size() ? rhs[i] : 0 );
		carry = v[i] >= base;
		if ( carry )
			v[i] -= base;
	}
	return *this;
}

BigUnsigned& BigUnsigned::operator-= ( const BigUnsigned& rhs )
{
	if( rhs > *this )
	{
		v.clear();		
		return *this;
	}
	value_type carry = 0;
	for ( size_t i = 0; i < rhs.v.size() || carry; ++i )
	{
		v[i] -= carry + ( i < rhs.v.size() ? rhs[i] : 0 );
		carry = v[i] < 0;
		if ( carry )
			v[i] += base;
	}
	while ( v.size() > 1 && v.back() == 0 )
		v.pop_back();
	return *this;
}

BigUnsigned& BigUnsigned::operator*= ( const BigUnsigned& rhs )
{
	return *this = *this * rhs;
}

BigUnsigned& BigUnsigned::operator/= ( const BigUnsigned& rhs )
{
	return *this = *this / rhs;
}

BigUnsigned& BigUnsigned::operator%= ( const BigUnsigned& rhs )
{
	return *this = *this % rhs;
}

BigUnsigned& BigUnsigned::operator<<= ( const BigUnsigned& rhs )
{
	return *this *= pow( 2, rhs );
}

BigUnsigned& BigUnsigned::operator>>= ( const BigUnsigned& rhs )
{
	return *this /= pow( 2, rhs );
}

BigUnsigned operator+ ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	BigUnsigned ret( lhs );
	return ret += rhs;
}

BigUnsigned operator- ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	BigUnsigned ret( lhs );
	ret -= rhs;
	return ret;
}

BigUnsigned operator* ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	BigUnsigned ret;
	ret.v.resize( lhs.v.size() + rhs.v.size() );

	for ( size_t i = 0; i < lhs.v.size(); ++i )
	{
		for ( size_t j = 0, carry = 0; j < rhs.v.size() || carry; ++j )
		{
			BigUnsigned::value_type cur = ret[i + j] + 
				lhs[i] * ( j < rhs.v.size() ? rhs[j] : 0 ) + carry;
			ret[i + j] = cur % BigUnsigned::base;
			carry = ( size_t ) cur /BigUnsigned::base; 
		} 
	}

	while ( ret.v.size() > 1 && !ret.v.back() )
		ret.v.pop_back();

	return ret;
}

BigUnsigned operator/ ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	if ( rhs.is_zero() )
		throw "BigUnsigned::operator /: division by zero"; 
	BigUnsigned ret, tmp;
	if ( rhs > lhs )
		return ret;
	for ( int i = ( int )lhs.v.size()-1; i >= 0; --i )
	{
		tmp.v.insert( tmp.v.begin(), lhs[i] );
		BigUnsigned::value_type l = 0, r = BigUnsigned::base;
		BigUnsigned::value_type x = 0, m;
		while ( l <= r )
		{
			m = ( l + r ) >> 1;
			BigUnsigned cur = rhs * m;
			if ( cur <= tmp )
			{
				x = m;
				l = m+1;
			}
			else
				r = m-1;
		}
		ret.v.push_back( x );
		tmp -= rhs * x;
	}
	std::reverse( ret.v.begin(), ret.v.end() );
	while ( ret.v.size() > 1 && ret.v.back() == 0 )
		ret.v.pop_back();
	return ret;
}

BigUnsigned operator% ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	if ( rhs.is_zero() )
		throw "BigUnsigned::operator %: division by zero"; 
	BigUnsigned ret;
	if ( rhs > lhs )
		return lhs;
	for ( int i = ( int )lhs.v.size()-1; i >= 0; --i )
	{
		ret.v.insert( ret.v.begin(), lhs[i] );
		BigUnsigned::value_type l = 0, r = BigUnsigned::base;
		BigUnsigned::value_type x = 0, m;
		while ( l <= r )
		{
			m = ( l + r ) >> 1;
			BigUnsigned cur = rhs * m;
			if ( cur <= ret )
			{
				x = m;
				l = m+1;
			}
			else
				r = m-1;
		}
		ret -= rhs * x;
	}		
	return ret;
}

BigUnsigned operator<< ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	BigUnsigned ret( lhs );
	return ret <<= rhs;
}

BigUnsigned operator>> ( const BigUnsigned& lhs, const BigUnsigned& rhs )
{
	BigUnsigned ret( lhs );
	return ret >>= rhs;
}

BigUnsigned BigUnsigned::operator+ ()
{
	return *this;
}

BigUnsigned BigUnsigned::operator- ()
{
	return BigUnsigned();
}

BigUnsigned& BigUnsigned::operator++ ()
{
	value_type carry = 1;
	for ( size_t i = 0; carry; ++i )
	{
		if ( i == v.size() )
			v.push_back( 0 );
		value_type cur = v[i] + carry;
		v[i] = cur % base;
		carry = cur / base;
	}	
	return *this;
}

BigUnsigned BigUnsigned::operator++ ( int )
{
	BigUnsigned tmp( *this );
	++( *this );
	return tmp;
}

BigUnsigned& BigUnsigned::operator-- ()
{
	if ( is_zero() )
		return *this;
	value_type carry = 1;
	for ( size_t i = 0; carry; ++i )
	{
		if ( i == v.size() )
			v.push_back( 0 );
		value_type cur = v[i] - carry;
		v[i] = cur % base;
		carry = cur / base;
	}	

	while ( v.size() > 1 && v.back() == 0 )
		v.pop_back();
	return *this;
}

BigUnsigned BigUnsigned::operator-- ( int )
{
	BigUnsigned tmp( *this );
	--( *this );
	return tmp;
}

bool operator== ( const BigUnsigned& lhs, const BigUnsigned& rhs ) 
{
	return lhs.v == rhs.v;
}

bool operator!= ( const BigUnsigned& lhs, const BigUnsigned& rhs ) 
{
	return !( lhs == rhs );
}

bool operator< ( const BigUnsigned& lhs, const BigUnsigned& rhs ) 
{
	if ( lhs.v.size() != rhs.v.size() )
		return lhs.v.size() < rhs.v.size();	
	int i = lhs.v.size();
	while ( --i > 0 && lhs[i] == rhs[i] ) 
		;
	return lhs[i] < rhs[i];
}

bool operator> ( const BigUnsigned& lhs, const BigUnsigned& rhs) 
{
	return rhs < lhs;
}

bool operator<= ( const BigUnsigned& lhs, const BigUnsigned& rhs) 
{
	return !( rhs < lhs );
}

bool operator>= ( const BigUnsigned& lhs, const BigUnsigned& rhs) 
{
	return !( lhs < rhs );
}

std::istream& operator>> ( std::istream& stream, BigUnsigned& rhs )
{
	std::string str;
	stream >> str;
	rhs = BigUnsigned( str );
	return stream;	
}

std::ostream& operator<< ( std::ostream& stream, const BigUnsigned& rhs )
{
	if ( rhs.v.empty() )
		stream << 0;
	else
		stream << rhs.v.back();
	int digits = static_cast<int>( log10( BigUnsigned::base ) );
	for ( int i = ( int )rhs.v.size() - 2; i >= 0; --i )
	{
		stream << std::setfill( '0' ) << std::setw( digits ) << rhs[i];
	}
	return stream;
}

#ifdef __GNUC__ 
explicit BigUnsigned::operator bool()  
{
	return is_zero();
}
#endif

BigUnsigned pow( const BigUnsigned& lhsn, const BigUnsigned& rhsn ) 
{
	BigUnsigned lhs(lhsn), rhs(rhsn);
	BigUnsigned ret( 1 );
	while ( !rhs.is_zero() ) 
	{
		if ( rhs.v.front() & 1 )
			ret *= lhs;
		lhs *= lhs;
		rhs /= 2;
	}
	return ret;
}

BigUnsigned sqrt( const BigUnsigned& x )
{
	BigUnsigned ret;
	size_t pos = ( x.v.size() + 1 ) / 2;
	ret.v.resize( pos ); 
	while ( pos-- )
	{
		BigUnsigned::value_type l = 0, r = BigUnsigned::base;
		BigUnsigned::value_type x = 0, m;
		while ( l <= r ) 
		{
			m = ( l + r ) >> 1;
			ret[pos] = m;
			if ( ret * ret <= x )
			{
				x = m;
				l = m + 1;
			}
			else
				r = m - 1;
		}
		ret[pos] = x;
	}
	while ( ret.v.size() > 1 && ret.v.back() == 0 )
		ret.v.pop_back();
	return ret;
}

bool BigUnsigned::is_zero() const
{
	return v.empty() || v.front() == 0;
}

BigUnsigned::value_type& BigUnsigned::operator[] ( size_t i )
{
	return v[i];
}

const BigUnsigned::value_type& BigUnsigned::operator[] ( size_t i ) const
{
	return v[i];
}