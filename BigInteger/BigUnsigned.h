#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include "mvector.h"

class BigUnsigned
{
public:
	BigUnsigned();
	BigUnsigned( const std::string& );
	BigUnsigned( const char* );
	template <typename T>
	BigUnsigned( T x, typename 
		std::enable_if<std::is_integral<T>::value>::type* = nullptr );	
	~BigUnsigned();

	BigUnsigned& operator+= ( const BigUnsigned& );
	BigUnsigned& operator-= ( const BigUnsigned& );
	BigUnsigned& operator*= ( const BigUnsigned& );
	BigUnsigned& operator/= ( const BigUnsigned& );
	BigUnsigned& operator%= ( const BigUnsigned& );
	BigUnsigned& operator<<= ( const BigUnsigned& );
	BigUnsigned& operator>>= ( const BigUnsigned& );

	friend BigUnsigned operator+ ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator- ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator* ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator/ ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator% ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator<< ( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned operator>> ( const BigUnsigned&, const BigUnsigned& );

	BigUnsigned operator+ ();
	BigUnsigned operator- ();
	BigUnsigned& operator++ ();
	BigUnsigned operator++ ( int );
	BigUnsigned& operator-- ();
	BigUnsigned operator-- ( int );

	friend bool operator== ( const BigUnsigned&, const BigUnsigned& );
	friend bool operator!=( const BigUnsigned&, const BigUnsigned& );
	friend bool operator< ( const BigUnsigned&, const BigUnsigned& );
	friend bool operator> ( const BigUnsigned&, const BigUnsigned& );
	friend bool operator<= ( const BigUnsigned&, const BigUnsigned& ); 
	friend bool operator>= ( const BigUnsigned&, const BigUnsigned& ); 

	friend std::istream& operator>> ( std::istream&, BigUnsigned& );
	friend std::ostream& operator<< ( std::ostream&, const BigUnsigned& );

	friend BigUnsigned pow( const BigUnsigned&, const BigUnsigned& );
	friend BigUnsigned sqrt( const BigUnsigned& );	

	bool is_zero() const;
#ifdef __GNUC__ 
	explicit operator bool() const;	
#endif
private:
	typedef long long value_type;
	value_type& operator[] ( size_t );
	const value_type& operator[] ( size_t ) const;
	mvector<value_type> v;
	static const value_type base = 1000 * 1000 * 1000;
};